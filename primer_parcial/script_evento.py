#!/usr/bin/env python

__author__ = "fedeagui94"
__license__ = "Public Domain"
__version__ = "full"
__email__ = "aguilerafede2@gmail.com"
__status__ = "Prototype"

from abc import ABCMeta, abstractmethod


class Evento (metaclass=ABCMeta)  :
	'''clase abstracta de evento'''
	cantidad_eventos = 0
	def __init__(self, cliente, fecha_evento, precio_barril, cantidad_barril):
		self.cliente  = ciente
		self.fecha_evento  = fecha_evento
		self.precio_barril = precio_barril
		self.cantidad_barril = cantidad_barril
		# Definimos el atributo costo_evento.
		self.costo_evento = 0
		# Incrementamos la cantidad de instancias
		Evento.cantidad_eventos += 1

	def  __str__(self):
		'''Genera la lista cliente, fecha_evento, ----------- costo'''
		# Antes de imprimir los datos debemos calcular el costo.
		self.calcular_costo()
		# Creamos la cadena con el formato "apellido, nombre"
		self.__ape_client = self.__cliente + ', ' + self.__fecha_evento
		#Generamos un formato de impresion para el sueldo.
		return '{:-<050} {:10n}'.format(self.__ape_client, self.costo_evento)

	@abstractmethod
	def calcular_costo(self):
		'''Metodo abstracto para el calculo del
		costo del evento segun el tipo de evento'''
		pass

	@staticmethod
	def obtener_total_eventos():
		'''Metodo estatico que retorna la cantidad
		de instancias de evento'''
		return Evento.cantidad_eventos


class EventoNormal(Evento):
	'''clase que representa a los eventos mas comunes'''
	def __init__(self,estado_reserva,*kwargs):
		super().__init__(**kwargs)
		self.estado_reserva = estado_reserva

	def calcular_costo (self):
		'''Implementacion del metodo abstracto para el
		calculo del costo del evento'''
		self.costo_evento = self.__cantidad_barriles * self.__precio_barril


class EventoAlquiler (Evento):
	'''clase que representa un evento que tiene alquiler de manijas extras'''
	def __init__(self,estado_reserva, cantidad_manijas_extras, costo_manijas_extras,**kwargs):
		super().__init__(**kwargs)
		self.estado_reserva = estado_reserva
		self.cantidad_manijas_extras = cantidad_manijas_extras
		self.costo_manijas_extras = costo_manijas_extras

	def calcular_costo (self) :
		self.costo_evento = (self.__cantidad_barriles * self.__precio_barril) + (self.__cantidad_manijas_extras * self.__costo_manijas_extras)


class EventoConsignacion (Evento ) :
	'''clase que representa un evento que tienen barriles a consignacion'''
	def __init__(self,estado_reserva, cantidad_bconsig, costo_bconsig, estado_consig, **kwargs):
		super().__init__(**kwargs)
		self.estado_reserva = estado_reserva
		self.cantidad_bconsig = cantidad_bconsig
		self.costo_bconsig = costo_bconsig
		self.estado_consig = estado_consig

	def calcular_costo (self):
		'''Implementacion del metodo abstracto para el
		calculo del costo del evento con barriles a consignacion'''
		if  self.estado_consig == 'SI':
		    self.costo_evento = (self.__cantidad_barriles * self.__precio_barril) + (self.__cantidad_bconsig * self.__costo_bconsig)
		else:
                    self.costo_evento = (self.__cantidad_barriles * self.__precio_barril)


class EventoAlquilerconsignacion (EventoAlquiler, EventoConsignacion) :
	'''clase que representa a los eventos que tienen alquiler de manijas y barriles a consignacion '''
	def __init__(self,estado_reserva, cantidad_manijas_extras, costo_manijas_extras, cantidad_bconsig, costo_bconsig, estado_consig,**kwargs):
		super().__init__(**kwargs)
		self.estado_reserva = estado_reserva
		self.cantidad_manijas_extras = cantidad_manijas_extras
		self.costo_manijas_extras = costo_manijas_extras
		self.cantidad_bconsig = cantidad_bconsig
		self.costo_bconsig = costo_bconsig
		self.estado_consig = estado_consig

	def calcular_costo (self) :
		if  self.estado_consig == 'SI':
			self.costo_evento = ((self.__cantidad_barriles * self.__precio_barril) + (self.__cantidad_bconsig * self.__costo_bconsig)) + (self.__cantidad_manijas_extras * self.__costo_manijas_extras)
		else:
			self.costo_evento = (self.__cantidad_barriles * self.__precio_barril) + (self.__cantidad_manijas_extras * self.__costo_manijas_extras)


class Aplicacion():
	'''Clase destinada a la ejecucion de la aplicacion segun el enunciado.'''
	# Definimos la lista de eventos.
	lista_eventos = []

	@staticmethod
	def cargar_lista_eventos():
		'''Crea instancias de eventos y los agrega en la lista.'''
		# Creamos los eventos.
		even1 = EventoNormal(JOSE, '25/03/20203', 30000, 3, 'PAGADO')
		even2 = EventoAlquiler(FEDE, '30/03/2023',30000, 4,'RESERVADO',5,25000)
		even3 = EventoConsignacion(LUIS, '27/03/2023', 50000, 6, 'PAGADO',2,30000,'SI')
		even4 = EventoAlquilerConsignacion(SILVIO, '31/03/2023', 30000,5,'RESERVADO',8,30000,2,60000,'SI')

		Aplicacion.lista_eventos.append(even1)
		Aplicacion.lista_eventos.append(even2)
		Aplicacion.lista_eventos.append(even3)
		Aplicacion.lista_eventos.append(even4)

	@staticmethod
	def generar_listado_eventos():
		'''Recorre la lista de eventos y los imprime con formato.'''
		# Imprimimos un encabezado para el listado
		print('Lista de Eventos'.center(65, '*'))
		# Iteramos sobre la lista para imprimir los costos.
		nro_linea = 0
		for empleado in Aplicacion.lista_eventos:
			nro_linea+=1
			print('{:2}- {}'.format(nro_linea, evento))
		# Imprimimos un resumen de los costos
		msg_resumen = 'Se muestra {} evento(s) de un total de {}'
		print(msg_resumen.format(nro_linea,
                                 Evento.obtener_total_eventos()).center(65,'*'))

	@staticmethod
	def main():
		'''Punto de inicio del programa'''
		Aplicacion.cargar_lista_eventos()
		Aplicacion.generar_listado_eventos()

if __name__ == '__main__':
	Aplicacion.main()

